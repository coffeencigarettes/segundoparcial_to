﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradingRoutesSimulation
{
    public class Merchant
    {
        public readonly Point Town;
        public readonly Point CapitalCity;

        public Point Position;
        public Point[] pathCapital, pathTown;

        public List<Point> targetPath;

        public Pathfinding pathfinder;

        public bool GoingToCapital = true;

        public Merchant(Point position, Point capital, TerrainType[,] map)
        {
            Town = Position = position;
            CapitalCity = capital;

            pathfinder = new Pathfinding(map);
            pathCapital = pathfinder.GetPath(Position, CapitalCity).ToArray();
            pathTown = pathfinder.GetPath(CapitalCity, Town).ToArray();
            targetPath = pathCapital.ToList();
        }

        public void UpdateOn(TerrainType[,] map)
        {
            //Point target = GoingToCapital ? CapitalCity : Town;
            //var pathfinder = new Pathfinding(map);
            //var path = pathfinder.GetPath(Position, target).Skip(1);
            if (targetPath.Any())
            {
                Position = targetPath.First();
                targetPath.RemoveAt(0);
            }
            else
            {
                if(GoingToCapital)
                {
                    targetPath = pathTown.ToList();
                    GoingToCapital = !GoingToCapital;
                }
                else
                {
                    targetPath = pathCapital.ToList();
                    GoingToCapital = !GoingToCapital;
                }
            }
        }
    }
}
